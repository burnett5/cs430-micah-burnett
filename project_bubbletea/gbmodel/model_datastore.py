# Google Cloud Datastore model backend code

from .Model import Model
from datetime import datetime
from google.cloud import datastore

def from_datastore(entity):
    """Translates Datastore results into the format expected by the
    application.

    Datastore typically returns:
        [Entity{key: (kind, id), prop: val, ...}]

    This returns:
        [ name, address, city, state, zip, hours, phone, rating, review ]
    where each item in the list is a Python string
    """
    if not entity:
        return None
    if isinstance(entity, list):
        entity = entity.pop()
    return [entity['name'],entity['address'],entity['city'],entity['state'],entity['zip'],
            entity['hours'],entity['phone'],entity['rating'],entity['review']]

class model(Model):
    def __init__(self):
        self.client = datastore.Client('cs430-micah-burnett')

    def select(self):
        # Use 'Shop' as kind for datastore config
        query = self.client.query(kind = 'Shop')
        entities = list(map(from_datastore,query.fetch()))
        return sorted(entities, key=lambda x: x[0]) # sort by shop name

    def insert(self,name,address,city,state,zip,hours,phone,rating,review):
        key = self.client.key('Shop')
        rev = datastore.Entity(key)
        rev.update( {
            'name': name,
            'address': address,
            'city': city,
            'state': state,
            'zip': zip,
            'hours': hours,
            'phone': phone,
            'rating': rating,
            'review': review
            })
        self.client.put(rev)
        return True
