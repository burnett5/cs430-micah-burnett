"""
Python list model
"""
from datetime import date
from .Model import Model

class model(Model):
    def __init__(self):
        self.locations = []

    def select(self):
        """
        Returns bubble tea locations list of lists
        Each list in locations contains: name, address, city, state, zip, hours, phone, rating, review
        :return: List of lists
        """
        return self.locations

    def insert(self, name, address, city, state, zip, hours, phone, rating, review):
        """
        Appends a new list of values representing new bubble tea shop into locations
        :param name: String
        :param address: String
        :param city: String
        :param state: String
        :param zip: String
        :param hours: String
        :param phone: String
        :param rating: String
        :param review: String
        :return: True
        """
        params = [name, address, city, state, zip, hours, phone, rating, review]
        self.locations.append(params)
        return True
