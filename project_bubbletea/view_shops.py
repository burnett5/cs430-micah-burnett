from flask import render_template
from flask.views import MethodView
import gbmodel

class ViewShops(MethodView):
    def get(self):
        """
        Handles GET requests by rendering
        list of bubble tea locations
        """
        model = gbmodel.get_model()
        entries = [dict(name=row[0], 
                        address=row[1], 
                        city=row[2], 
                        state=row[3],
                        zip=row[4],
                        hours=row[5],
                        phone=row[6],
                        rating=row[7],
                        review=row[8] ) for row in model.select()]
        return render_template('view_shops.html',entries=entries)