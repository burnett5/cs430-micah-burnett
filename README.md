## CS430 Internet, Web, and Cloud Systems Repository

This repository contains the code for my CS430 Cloud Systems coursework. Of main interest
is the Recipe Roulette web app code which can be found within the /final directory. To download
and deploy the app on GCP, follow the instructions below.

1. Set up a Google Cloud Platform (GCP) account and create a new project.
2. Start a cloud shell session and ensure that you are set to the current project.
3. Within the shell, clone the repository and CD into /final
4. Set up a virtual environment with the command: `virtualenv -p python3 env`
5. Activate the environment by running `source env/bin/activate`
6. Download any dependencies with `pip3 install -r requirements.txt`
7. Create a RapidAPI account and subscripe to the Spoonacular API (Base plan is OK).
8. Copy your API key and replace "YOUR_KEY_HERE" in ".env_sample". Rename file to ".env".
9. Check to make sure Datastore is the selected model in **/model/_\_init_\_.py** and replace my GCP project ID with your own.
10. Run `python app.py` to test the app first on the dev server.
11. Deploy the app with the command `gcloud app deploy`.

You can find my deployment of the app [HERE](https://recipe-app-277023.wl.r.appspot.com/)

