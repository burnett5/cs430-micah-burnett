class Model():
    def select(self):
        """
        Gets all entries from the database
        :return: Tuple containing all rows of database
        """
        pass

    def insert(self, name, address, city, state, zip, hours, phone, rating, review):
        """
        Inserts entry into database
        :param name: String
        :param address: String
        :param city: String
        :param state: String
        :param zip: String
        :param hours: String
        :param phone: String
        :param rating: String
        :param review: String
        :return: none
        :raises: Database errors on connection and insertion
        """
        pass
