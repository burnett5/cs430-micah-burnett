"""
A simple bubble tea tracker flask app.
"""
import flask
from flask.views import MethodView
from index import Index
from add_shop import AddShop
from view_shops import ViewShops

app = flask.Flask(__name__)       # our Flask app

app.add_url_rule('/',
                 view_func=Index.as_view('index'))

app.add_url_rule('/add-shop/',
                 view_func=AddShop.as_view('add-shop'),
                 methods=['GET', 'POST'])

app.add_url_rule('/view-shops/',
                 view_func=ViewShops.as_view('view-shops'),
                 methods=['GET'])     

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)
