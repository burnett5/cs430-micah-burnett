"""
Developed by Micah Burnett for 'CS430 Internet, Web and Cloud Systems' at PSU
Last updated 12/10/2019

Recipe Roulette is a python flask web app which lets users generate custom meal plans 
based on their dietary needs and preferences. It was engineered to run on Google's 
App Engine platform. It supports a simple python list, sqlite3, or Cloud Datastore for
backend configs. Datastore is recommended to allow for scaling.
"""

import flask
from flask.views import MethodView
from index import Index
from my_meals import MyMeals
from details import Details

app = flask.Flask(__name__)       # our Flask app
app.config.from_pyfile('config.py')

# Homepage
app.add_url_rule('/',
                 view_func=Index.as_view('index'),
                 methods=['GET', 'POST'])

# View stored meals and plans
app.add_url_rule('/my-meals/',
                 view_func=MyMeals.as_view('my-meals'),
                 methods=['GET'])

# Recipe page with ingredients and instructions
app.add_url_rule('/details/',
                 view_func=Details.as_view('details'),
                 methods=['GET', 'POST'])

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000, debug=True)
