from os import getenv, environ
from dotenv import load_dotenv

load_dotenv()

API_KEY = environ.get("API_KEY")