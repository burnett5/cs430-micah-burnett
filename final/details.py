"""
The details view is responsible for displaying a single recipe, 
including its ingredients list and instructions.
"""

from flask import render_template, request, redirect, url_for, current_app
from flask.views import MethodView
import model
import requests
import ast

# Globals to save last API responses
last_meal = None
last_details = None

class Details(MethodView):
    def get(self):
        """
        Render recipe image, ingredients, and instructions
        """

        global last_meal, last_details

        # If meal arg not set, then use the last saved API responses (ex: if 'back' button is pressed)
        meal = request.args.get('meal')
        if meal is None:
            meal = last_meal
            details = last_details
        else:
            meal_dict = ast.literal_eval(meal)  # Convert string to dict

            id = meal_dict['id']
            url = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/" + str(id) + "/information"

            headers = {                                                                
                'x-rapidapi-host': "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
                'x-rapidapi-key': current_app.config['API_KEY']
            }

            details = requests.request("GET", url, headers=headers).json()
            ingredients = details['extendedIngredients']
            ingredient_list = []
            for ingredient in ingredients:
                ingredient_list.append(ingredient['original'])

            last_meal = meal_dict
            last_details = details

        return render_template('details.html', details=details, meal=last_meal)

    def post(self):
        """
        When 'favorite' button is pressed, fetch hidden form fields
        and add recipe info to model.
        """
        my_model = model.get_model()
        my_model.insert_meal(
            request.form['id'],
            request.form['title'],
            request.form['readyInMinutes'],
            request.form['servings'],
            request.form['summary'],
            request.form['image']
        )

        return redirect(url_for('details'))
