"""
MyMeals view is responsible for displaying the users favorite meals and
meal plans. There is currently no way to delete items.
"""

from flask import render_template
from flask.views import MethodView
import model

class MyMeals(MethodView):
    def get(self):
        my_model = model.get_model()
        entries = [dict(id=row[0],
                        title=row[1],
                        readyInMinutes=row[2],
                        servings=row[3],
                        summary=row[4],
                        image=row[5]) for row in my_model.select_meals()]

        plans = [dict(meals=[dict(id=row[0], title=row[1], readyInMinutes=row[2], servings=row[3], summary=row[4], image=row[5]),
                             dict(id=row[6], title=row[7], readyInMinutes=row[8], servings=row[9], summary=row[10], image=row[11]),
                             dict(id=row[12], title=row[13], readyInMinutes=row[14], servings=row[15], summary=row[16], image=row[17])],
                      nutrients=dict(calories=row[18], protein=row[19], fat=row[20], carbohydrates=row[21])) for row in my_model.select_plans()]

        return render_template('my-meals.html', entries=entries, plans=plans)
