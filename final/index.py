"""
Index is the homepage view of the application. It contains the recipe
generator form and populates with 3 recipes matching the users form data (calories
and diet plan). The user may also submit a blank or incomplete form and
random recipes will be pulled. The user can favorite a particular recipe or
meal plan.
"""

from flask import render_template, request, current_app
from flask.views import MethodView
import requests
import model

# Save last API response to reload page with original data
last_response = None

class Index(MethodView):
    def get(self):
        return render_template('index.html')

    def post(self):
        """
        When user clicks 'generate' button, fetch recipes matching criteria.
        When user clicks 'favorite' button, fetch hidden form fields and store recipe data in model.
        When user clicks 'add plan' button, fetch hidden form fields and store plan data in model.
        """
        if request.form['submit'] == 'Generate':
            querystring = {"timeFrame":"day"}
            calories = request.form['dailyCal']
            diet = request.form['diet']

            # Append whatever data user submits to querystring
            if not calories == "":
                querystring['targetCalories'] = calories
            if not diet == "":
                querystring['diet'] = diet

            url = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/mealplans/generate"
            
            headers = {
                'x-rapidapi-host': "spoonacular-recipe-food-nutrition-v1.p.rapidapi.com",
                'x-rapidapi-key': current_app.config['API_KEY']
            }

            response = requests.request("GET", url, headers=headers, params=querystring)
            response_data = response.json()

            # Append summary and image path of each recipe into response object
            for mealNo, meal in enumerate(response_data['meals'], start=0):
                url = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com/recipes/" + str(meal['id']) + "/summary"
                response2 = requests.request("GET", url, headers=headers).json()
                meal['summary'] = response2['summary']
                meal['image'] = f"{meal['id']}-312x150.{meal['imageType']}"

            global last_response
            last_response = response_data

        elif request.form['submit'] == 'Favorite':
            my_model = model.get_model()
            my_model.insert_meal(
                request.form['id'],
                request.form['title'],
                request.form['readyInMinutes'],
                request.form['servings'],
                request.form['summary'],
                request.form['image']
            )

        elif request.form['submit'] == 'Add Plan':
            my_model = model.get_model()
            my_model.insert_plan(
                last_response['meals'][0],
                last_response['meals'][1],
                last_response['meals'][2],
                last_response['nutrients']
            )

        return render_template('index.html', response=last_response)