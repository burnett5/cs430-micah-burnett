"""
Google Cloud Datastore model backend code. NOTE: This code has been modified
from the guestbook code found here: https://bitbucket.org/wuchangfeng/cs430-src/src/master/
"""

from .Model import Model
from datetime import datetime
from google.cloud import datastore

def from_datastore(entity, kind):
    """Translates Datastore results into the format expected by the
    application.

    Datastore typically returns:
        [Entity{key: (kind, id), prop: val, ...}]

    This returns:
        (kind == Meals):
        [ id, title, readyInMinutes, servings, summary, image ] or...
        (kind == Plans):
        [ id1, title1, readyInMinutes1, servings1, summary1, image1, ..., calories, protein, carbohydrates, date]
    
    where each item in the list is a Python string
    """
    if not entity:
        return None
    if isinstance(entity, list):
        entity = entity.pop()
    if kind == "Meals":
        return [entity['id'],entity['title'],entity['readyInMinutes'],entity['servings'],entity['summary'],entity['image']]
    elif kind == "Plans":
        return [entity['id1'],entity['title1'],entity['readyInMinutes1'],entity['servings1'],entity['summary1'],entity['image1'],
                entity['id2'],entity['title2'],entity['readyInMinutes2'],entity['servings2'],entity['summary2'],entity['image2'],
                entity['id3'],entity['title3'],entity['readyInMinutes3'],entity['servings3'],entity['summary3'],entity['image3'],
                entity['calories'],entity['protein'],entity['fat'],entity['carbohydrates'],entity['date']]

class model(Model):
    def __init__(self):
        self.client = datastore.Client('cs430-micah-burnett')

    def select_meals(self):
        query = self.client.query(kind = 'Meals')
        entities = list(from_datastore(meal, "Meals") for meal in query.fetch())
        return sorted(entities, key=lambda x: x[1]) # sort by meal title

    def select_plans(self):
        query = self.client.query(kind = 'Plans')
        entities = list(from_datastore(plan, "Plans") for plan in query.fetch())
        return sorted(entities, key=lambda x: x[22]) # sort by date added

    def insert_meal(self, id, title, readyInMinutes, servings, summary, image):
        key = self.client.key('Meals')
        rev = datastore.Entity(key)
        rev.update( {
            'id': id,
            'title': title,
            'readyInMinutes': readyInMinutes,
            'servings': servings,
            'summary': summary,
            'image': image
            })
        self.client.put(rev)
        return True
        
    def insert_plan(self, meal1, meal2, meal3, nutrients):
        key = self.client.key('Plans')
        rev = datastore.Entity(key)
        rev.update( {
            'id1': meal1['id'],
            'title1': meal1['title'],
            'readyInMinutes1': meal1['readyInMinutes'],
            'servings1': meal1['servings'],
            'summary1': meal1['summary'],
            'image1': meal1['image'],
            'id2': meal2['id'],
            'title2': meal2['title'],
            'readyInMinutes2': meal2['readyInMinutes'],
            'servings2': meal2['servings'],
            'summary2': meal2['summary'],
            'image2': meal2['image'],
            'id3': meal3['id'],
            'title3': meal3['title'],
            'readyInMinutes3': meal3['readyInMinutes'],
            'servings3': meal3['servings'],
            'summary3': meal3['summary'],
            'image3': meal3['image'],
            'calories': nutrients['calories'],
            'protein': nutrients['protein'],
            'fat': nutrients['fat'],
            'carbohydrates': nutrients['carbohydrates'],
            'date': datetime.today()
            })
        self.client.put(rev)
        return True
