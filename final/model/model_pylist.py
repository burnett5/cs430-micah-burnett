"""
Python list model. NOTE: This code has been modifiedfrom the guestbook
code found here: https://bitbucket.org/wuchangfeng/cs430-src/src/master/
"""

from .Model import Model

class model(Model):
    def __init__(self):
        self.meals = []
        self.plans = []

    def select_meals(self):
        """
        Returns Meals list of lists
        Each list in Meals contains: id, title, readyInMinutes, servings, summary, image
        :return: List of lists
        """
        return self.meals

    def select_plans(self):
        """
        Returns Plans list of lists
        Each list in Plans contains: id1, title1, readyInMinutes1, servings1, summary1, image1,
                                     id2, title2, readyInMinutes2, servings2, summary2, image2,
                                     id3, title3, readyInMinutes3, servings3, summary3, image3,
                                     calories, protein, fat, carbohydrates
        :return: List of lists
        """
        return self.plans

    def insert_meal(self, id, title, readyInMinutes, servings, summary, image):
        """
        Appends a new list of values representing new meal into meals
        :param id: String
        :param title: String
        :param readyInMinutes: String
        :param servings: String
        :param summary: String
        :param image: String
        :return: True
        """
        params = [id, title, readyInMinutes, servings, summary, image]
        self.meals.append(params)
        return True

    def insert_plan(self, meal1, meal2, meal3, nutrients):
        """
        Appends a new list of values representing new plan into plans
        :param meal1: Dictionary
        :param meal2: Dictionary
        :param meal3: Dictionary
        :param nutrients: Dictionary
        :return: True
        """
        params = [meal1['id'], meal1['title'], meal1['readyInMinutes'], meal1['servings'], meal1['summary'], meal1['image'],
                  meal2['id'], meal2['title'], meal2['readyInMinutes'], meal2['servings'], meal2['summary'], meal2['image'],
                  meal3['id'], meal3['title'], meal3['readyInMinutes'], meal3['servings'], meal3['summary'], meal3['image'],
                  nutrients['calories'], nutrients['protein'], nutrients['fat'], nutrients['carbohydrates']]
        self.plans.append(params)
        return True
