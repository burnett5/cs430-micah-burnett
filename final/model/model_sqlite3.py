"""
Sqlite3 data model. NOTE: This code has been modifiedfrom the guestbook
code found here: https://bitbucket.org/wuchangfeng/cs430-src/src/master/

Data is stored in a SQLite database that looks something like the following:

meals:
+--------+---------------+----------------+----------+-----------------+-----------+
|   Id   |      Title    | ReadyInMinutes | Servings | Summary         | Image     |
+========+===============+================+==========+=================+===========+
| 654328 | Mac 'n Cheese | 45             | 2        | Cheesy goodness | [URL]     |
+--------+---------------+----------------+----------+-----------------+-----------+

plans:
+---------+-----+--------+-----+--------+----------+---------+-----+---------------+
|   Id1   | ... | Image1 | ... | Image3 | Calories | Protein | Fat | Carbohydrates |
+=========+==============+================+==========+=================+===========+
| 654328  | ... | [URL]  | ... | [URL]  | 2        | 54      | 23  | 60            |
+---------+-----+--------+-----+--------+----------+---------+-----+---------------+

"""

from .Model import Model
import sqlite3
DB_FILE = 'entries.db' # file for our database

class model(Model):
    def __init__(self):
        # Make sure our database exists. If not, create it.
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from meals")
        except sqlite3.OperationalError:
            cursor.execute("create table meals (id text, title text, readyInMinutes text, servings text, summary text, image text)")

        try:
            cursor.execute("select count(rowid) from plans")
        except sqlite3.OperationalError:
            cursor.execute("create table plans (id1 text, title1 text, readyInMinutes1 text, servings1 text, summary1 text, image1 text, \
                                                id2 text, title2 text, readyInMinutes2 text, servings2 text, summary2 text, image2 text, \
                                                id3 text, title3 text, readyInMinutes3 text, servings3 text, summary3 text, image3 text, \
                                                calories text, protein text, fat text, carbohydrates text)")
        cursor.close()

    def select_meals(self):
        """
        Gets all rows from the database
        Each row contains: id, title, readyInMinutes, servings, summary, image
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM meals")
        return cursor.fetchall()

    def select_plans(self):
        """
        Gets all rows from the database
        Each row contains: id1, title1, readyInMinutes1, servings1, summary1, image1, ... id3, ..., image3,
                           calories, protein, fat, carbohydrates
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM plans")
        return cursor.fetchall()

    def insert_meal(self, id, title, readyInMinutes, servings, summary, image):
        """
        Inserts entry into database
        :param id: String
        :param title: String
        :param readyInMinutes: String
        :param servings: String
        :param summary: String
        :param image: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = { 'id':id, 'title':title, 'readyInMinutes':readyInMinutes, 'servings':servings, 'summary':summary, 'image':image }
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into meals (id, title, readyInMinutes, servings, summary, image) VALUES \
                                          (:id, :title, :readyInMinutes, :servings, :summary, :image)", params)

        connection.commit()
        cursor.close()
        return True

    def insert_plan(self, meal1, meal2, meal3, nutrients):
        """
        Inserts entry into database
        :param meal1: Dictionary
        :param meal2: Dictionary
        :param meal3: Dictionary
        :param nutrients: Dictionary
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = { 'id1':meal1['id'], 'title1':meal1['title'], 'readyInMinutes1':meal1['readyInMinutes'], 'servings1':meal1['servings'], 'summary1':meal1['summary'], 'image1':meal1['image'],
                   'id2':meal2['id'], 'title2':meal2['title'], 'readyInMinutes2':meal2['readyInMinutes'], 'servings2':meal2['servings'], 'summary2':meal2['summary'], 'image2':meal2['image'],
                   'id3':meal3['id'], 'title3':meal3['title'], 'readyInMinutes3':meal3['readyInMinutes'], 'servings3':meal3['servings'], 'summary3':meal3['summary'], 'image3':meal3['image'],
                   'calories':nutrients['calories'], 'protein':nutrients['protein'], 'fat':nutrients['fat'], 'carbohydrates':nutrients['carbohydrates'] }
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()

        cursor.execute("insert into plans (id1, title1, readyInMinutes1, servings1, summary1, image1, \
                                           id2, title2, readyInMinutes2, servings2, summary2, image2, \
                                           id3, title3, readyInMinutes3, servings3, summary3, image3, \
                                           calories, protein, fat, carbohydrates) VALUES \
                                          (:id1, :title1, :readyInMinutes1, :servings1, :summary1, :image1, \
                                           :id2, :title2, :readyInMinutes2, :servings2, :summary2, :image2, \
                                           :id3, :title3, :readyInMinutes3, :servings3, :summary3, :image3, \
                                           :calories, :protein, :fat, :carbohydrates)", params)

        connection.commit()
        cursor.close()
        return True
