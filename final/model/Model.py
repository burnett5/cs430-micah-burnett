"""
Abstract base class for our data model API. NOTE: This code has been modified from
the guestbook code found here: https://bitbucket.org/wuchangfeng/cs430-src/src/master/
"""

class Model():
    def select_meals(self):
        """
        Gets all meal entries from the database
        :return: Tuple containing all rows of database
        """
        pass

    def select_plans(self):
        """
        Gets all plan entries from the database
        :return: Tuple containing all rows of database
        """
        pass

    def insert_meal(self, id, title, readyInMinutes, servings, summary, image):
        """
        Inserts meal entry into database
        :param id: String
        :param title: String
        :param readyInMinutes: String
        :param servings: String
        :param summary: String
        :param image: String
        :return: none
        :raises: Database errors on connection and insertion
        """
        pass

    def insert_plan(self, meal1, meal2, meal3, nutrients):
        """
        Inserts plan entry into database
        :param meal1: Dictionary
        :param meal2: Dictionary
        :param meal3: Dictionary
        :param nutrients: Dictionary
        :return: none
        :raises: Database errors on connection and insertion
        """
        pass