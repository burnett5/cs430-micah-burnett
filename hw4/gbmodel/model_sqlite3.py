"""
A simple bubble tea tracker flask app.
Data is stored in a SQLite database that looks something like the following:

+-------+-------------------+----------+-------+-------+---------+----------------+---------+--------+
| Name  | Address           | City     | State | Zip   | Hours   | Phone          | Rating  | Review |
+=======+===================+==========+=======+=======+=========+================+=========+========+
| BTea  | 386 NE Hampton St | Portland | OR    | 97229 | M-F 9-5 | (503) 643-2876 | 2 stars | Bad    |
+-------+-------------------+----------+-------+-------+---------+----------------+---------+--------+

This can be created with the following SQL (see bottom of this file):

    create table locations (name text, address text, city text, state text, \
                            zip text, hours text, phone text, rating text, review text)");
"""
from datetime import date
from .Model import Model
import sqlite3
DB_FILE = 'entries.db'    # file for our Database

class model(Model):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from locations")
        except sqlite3.OperationalError:
            cursor.execute("create table locations (name text, address text, city text, state text, \
                            zip text, hours text, phone text, rating text, review text)")
        cursor.close()

    def select(self):
        """
        Gets all rows from the database
        Each row contains: name, address, city, state, zip, hours, phone, rating, review
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM locations")
        return cursor.fetchall()

    def insert(self, name, address, city, state, zip, hours, phone, rating, review):
        """
        Inserts entry into database
        :param name: String
        :param address: String
        :param city: String
        :param state: String
        :param zip: String
        :param hours: String
        :param phone: String
        :param rating: String
        :param review: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'name':name, 'address':address, 'city':city, 'state':state, 'zip':zip, \
                  'hours':hours, 'phone':phone, 'rating':rating, 'review':review }
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into locations (name, address, city, state, zip, hours, phone, rating, review) VALUES \
                                             (:name, :address, :city, :state, :zip, :hours, :phone, :rating, :review)", params)

        connection.commit()
        cursor.close()
        return True
